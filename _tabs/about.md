---
# the default layout is 'page'
icon: fas fa-info-circle
order: 4
---
  

# About Rayken Wong

I was born and raised in Alberta, Canada. My education was primarily in Biological Sciences but that was in the before times. My current interests are primarily focused around computers. 

I am a firm believer in the open-source movement. Perhaps this is a result of my education in the sciences, but I believe that the fastest way for any field to progress is to share. By sharing our progress, we can avoid reinventing the wheel. Not restricting the use and development of shared code means that human creativity can truly innovate upon our works. Small can become big. Simple can become complex. Singular can become myriad. 


# About this website

This is my first website, hosted on GitLab pages and using Jekyll and the ["Chirpy" Jekyll theme by Cotes](https://github.com/cotes2020/jekyll-theme-chirpy). Many thanks to Cotes for creating such a wonderful and user-friendly Jekyll theme. You'll note that the first 4 posts are actually written by Cotes. Normally, you'd probably remove these posts but I'm keeping them there as a reminder about who created the theme as well as a static reference for me about tinkering with the theme, how to write markdown, *etc*.  

The current intended purpose of this website is a documentation hub for my homelab. The website will be a log of my homelab setup to that me or anyone can refer to. 
  
The secondary purpose of the website is to act as a medium for my creative projects. I hope that it will push me to consistently update this website and, by extension, help me grow.

<!-- > Add Markdown syntax content to file `_tabs/about.md`{: .filepath } and it will show up on this page.
{: .prompt-tip } -->
