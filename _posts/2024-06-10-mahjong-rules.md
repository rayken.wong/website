---
title: Mahjong rules
author: rayken
date: 2024-06-10 23:32:00
categories: [Blog, gaming]
tags: [mahjong]
---

# Mahjong Rules Cards
[Here](/post-assets/2024.06.10/Mahjong_2024-08-30.pdf) is a link to the mahjong rules that I created for gaming.

# Basics
## Tiles
 - There are 4 identical copies of each tile in the game except for flowers (see below)
 - There are 6 'suits' of tiles
    - Sticks
        - numbered 1 to 9 (1 looks like a bird)
        - looks like sticks with the count being the number
    - Circles
        - numbered 1 to 9
        - looks like circles with the count being the  number
    - Ten-Thousands
        - numbered 1 to 9
        - looks like two chinese characters stacked atop eachother. The top character denotes the number (see page 1 of the pdf for a translation)
    - Winds
        - one for each of the cardinal directions, written with the Chinese character for each direction
        - East, South, West, North
    - Dragons
        - 3 types
        - White, Red, Green
    - Flowers
        - Two types, Chinese numerals, Arabic numerals
        - Each type has 4 tiles numbered 1-4
        - Not used to build hands
## Hands
The goal of the game is to complete a 14 tile hand consisting of 4 sets-of-3 and a pair. The players play the game with 13 tiles for the most
### Sets-of-3
 - there are three types of sets:
    - Shang
        - Three consecutive numbers of the same suit
        - Only available to the Sticks, Circles and Ten-Thousands suits
        - Final tile of the set can only be taken from a discard if the discard is from the player to the left
    - Pong
        - Three identical tiles
        - Available to all playable suits
        - Final tile of the set can be taken from any discard. In case of conflict with Shang, person who calls Pong gets the tile
    - Gong
        - Four identical tiles
        - Available to all playable suits
        - Grab a tile from the bottom of the deck.
        - Final tile of the set can be taken from any discard. In case of conflict with Shang, person who calls Gong gets the tile

### Scoring of hands
Honestly, it's quite complicated, if you're just starting out, just whoever completes their 14 tile hand wins.

## Setup
1. Pour out the tiles onto your table face-down and shuffle them
2. Arrange the tiles into a single row 18 tiles wide two tiles tall.
3. Push your stack forwards. The right side of your stack should butt up against the stack of the player to your right. The left side of the stack should be free. (see the diagram on the top left corner of the second page of the above pdf)
4. Have someone roll the 3 dice to determine who goes first.
    - Sum the total of the sides facing up
    - Start counting counter clockwise with the person-to-the-right being 1, until you reach the number shown by the dice
    - The person's stack upon which you land will be the dealer
5. Give the dealer the dice and the direction indicator
6. Starting from the butt end, count out the number that was previously rolled and grab the next 4 tiles
    - Example:
        - The stack will be 18 tiles wide
        - If you roll a 9, count nine tiles from the butt end
        - Split the deck here
            - the tiles to the right will be the 'bottom' of the deck
            - the tiles to the left will be the 'top' of the deck
        - Grab the four tiles from the top of the deck (the tiles that would be underneath columns 10 and 11)
7. In a counter-clockwise order have the remaining players grab tiles in sets of four, always working towards the free end of the row and then moving onto the row (should the the row to the left) until each player has 12 tiles
8. Starting with the dealer, each player will take a single tile off the free end. Once each player has taken a tile, the dealer will take a second tile.
9. Flip the tiles towards yourself (don't show your hands)
10. Starting with the dealer, and moving counter-clockwise, reveal any flower tiles you have and grab an equivalent from the remains of the butt end of the stack that you started the game from. If you pick up an additional flower at this time, wait until all players have grabbed all their flowers before grabbing your additional one.
11. Dealer begins the game by discarding a single tile

### Setup Notes
 - If the dealer wins the game, the dice remains with them
  - If the dealer loses the game, the dice gets passed to the person to the right
  - Once the dice return to the original dealer, flip the wind indicator to the next wind (East, South, West, North)
  - Where to split the deck is always determined by a dice roll at the beginning of the round by the dealer
  - Wind positions are as follows (counter-clockwise)
    - East (Dealer is always east)
    - South
    - West
    - North

## Gameplay
1. Once a tile has been discarded, each of the players has the opportunity to claim that tile if it can complete a set-of-3. 
    - If someone claims the tile:
        - the turn is theirs, *ie.* if the player across from the discarder Pongs, the player to the right's turn has been skipped
        - they must reveal the set-of-3
        - they must discard a tile (unless they've won)
    - If no one claims the tile:
        - the turn moves to the next player (counterclockwise)
        - the discarded tile is dead and may no longer be picked up
2. This continues until someone has won or there are only 16 tiles left in the deck (draw condition, dealer keeps the dice)