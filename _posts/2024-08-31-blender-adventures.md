---
title: Blender Adventures
author: rayken
date: 2024-08-31 10:38:00
categories: [Blog, Blender, 3d printing]
tags: [Blender, 3d printing]
---

# Blender Adventures

So, I decided to purchase a 3d printer. I was initially intending to just purchase a 3d printed part (large fan shroud for my JBOD to increase airflow across my hard drives) but when I got the quote for the part, it was verging on the price of a printer itself. Of course, as is my MO, I ended up going all in and probably would have been better served by just buying the part.

In any case I've started learning how to use Blender to create objects that I might want to 3d print in the future. I'll also be trying my hand at FreeCAD as I believe that is a superior program for some of the things I wish to create. My choice of Blender was influenced by two main factors. Firstly, I wanted to use some open source software. Next, I desired a mature software platform. Blender checked those two boxes.

I would like to extend a big thanks to my friend Brad, who graciously gave me his old Wacom tablet. The tablet has made Blender even more intuitive than before. There's probably a way but, I wish there was a way to setup different shortcut keys for each mode Blender mode (something to look into at a future date)

My first foray into the using Blender was via the Sculpt mode. I love sculpt mode. I think it fits me very well as an artists tool. To have something instantly slowly transform from a sphere to something that matches the final form is very gratifying. In comparison, I find that with Inkscape, I get hung up on trying to get things *juuust* right; I find Blender sculpt-mode to be a much more organic art flow for me. Note, this not a condemnation of Inkscape, more of a criticism of how my brain works. 

Well, long story short, I've created two things 