---
title: My attempt at a personalized logo
author: rayken
date: 2024-06-10 23:39:00
categories: [Blog, art]
tags: [art, logo, Hermit the Crab]
---

# Logo
![Hermit the Crab](/post-assets/2024-06-10.02/Hermit%20the%20Crab%20Logo.jpg){: width="972" height="589" .w-50 .left}I had the idea to create a logo for myself, something that I could use to mark my work without necessarily having to attach my name to it. Of course, I have very little artistic ability so take everything below with a grain of salt. 

The concept is based off a quick joke I had with a coworker. We were discussing Muppets and I thought, instead of Kermit the Frog, I would be Hermit the Crab. A creature that seemed to fit my self image of a loner and a bit of a social coward. 

The drawing itself was created with Inkscape and based off of a photo of a hermit crab that I found on the internet. While I would like to credit the image, I can't seem to find it again. I will update the post once I do. The technique for drawing the conical shell was from a YouTube video titled: [Pen and Ink Drawing Tutorials \| How to draw sea shells](https://www.youtube.com/watch?v=yjlQoKuqXwo) by Alphonso Dunn.

Overall I do like the look of the crab. I used a lot of floating lines that don't connect with each other. This is the first time ding this but I do like the look. I used Fibonnaci's sequence to determine the height of each of the main shell's tiers. There's a little math everywhere in the natural world and I wanted to reflect that. I used a spiral shell theme for the wizards hat and I added stars in the shapes of constellations \(one of them being, of course, cancer\). I don't know why but I think they eyes turned out particularly well.

Printed out, the logo doesn't work. Future iterations will implement the following changes:

1. Make all the lines significantly thicker. When shrunk down to the size appropriate for a creator's mark the lines end up appearing quite thin.
2. Remove the constellations. Again, upon shrinking the image the purpose of the constellations were lost. The constellations were added to:
    - create a couple fun in-jokes like the cancer constellation
    - subtly provide information about myself
    - add a bit of mystique to the image so it fit the theme of a magical hermit of fantasy
3. You can't tell in the image I've uploaded here because I flattened it into a jpg but instead of properly clipping certain objects, I opted to merely fill certain objects with white in order to overlap them. Printed on white paper, this isn't an issue. When uploading to a website that has dark mode enabled, it looks all sorts of messed up. 